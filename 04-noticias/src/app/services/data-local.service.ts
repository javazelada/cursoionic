import { Injectable } from '@angular/core';
import { Article } from '../interfaces/interfaces';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ToastController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  noticias: Article[] = [];

  constructor(private nativeStorage: NativeStorage, private toastController: ToastController) { 
    this.cargarFavoritos();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  guardarNoticia(noticia: Article) {

    const existe = this.noticias.find( noti => noti.title === noticia.title );
    if ( !existe ) {
      this.noticias.unshift( noticia );
      this.nativeStorage.setItem('favoritos', this.noticias)
      .then(
        () => console.log('Stored item!'),
        error => console.error('Error storing item', error)
      );
    }
    this.presentToast('Agregando a favoritos');

  }

  async cargarFavoritos() {
    const favoritos = await this.nativeStorage.getItem('favoritos');
    if (favoritos) {
      this.noticias = favoritos;
    }
  }

  async borrarNoticia(noticia: Article) {
    this.noticias = this.noticias.filter(noti => noti.title !== noticia.title);
    this.nativeStorage.setItem('favoritos', this.noticias)
    this.presentToast('Borrado de favoritos');
  }
}
