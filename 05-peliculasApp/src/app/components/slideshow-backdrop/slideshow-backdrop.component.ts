import { Component, OnInit, Input } from '@angular/core';
import { Pelicula } from '../../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DetalleComponent } from '../detalle/detalle.component';
import { ComponentsModule } from '../components.module';

@Component({
  selector: 'app-slideshow-backdrop',
  templateUrl: './slideshow-backdrop.component.html',
  styleUrls: ['./slideshow-backdrop.component.scss'],
})
export class SlideshowBackdropComponent implements OnInit {

  @Input() peliculas: Pelicula[]= [];

  slideOpts = {
    // initialSlide: 0,
    // direction: 'horizontal',
    // speed: 300,
    // spaceBetween: 8,
    slidesPerView: 1.3,
    freeMode: true,
    // loop: true
  };

  constructor(private modalController: ModalController) { }

  ngOnInit() {}

  async verDetalle(id: string) {
    const modal = await this.modalController.create({
      component: DetalleComponent,
      componentProps: {
        id
      }
    });

    modal.present();
  }

}
