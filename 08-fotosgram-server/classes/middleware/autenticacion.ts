import { NextFunction, Response, Request } from 'express';
import Token from '../token';
import { decode } from 'jsonwebtoken';
import { Usuario } from '../../models/usuario.model';


export const verificacionToken = ( req: any, res: Response, next: NextFunction) => {

    const userToken = req.get('x-token') || '';
    Token.comprobarToken( userToken )
        .then( (decoded: any) => {
            console.log('Decoded', decoded);
            req.usuario = decoded.usuario;
            next();
        })
        .catch( err => {
            res.json({
                ok: false,
                mensaje: 'Token no es correcto'
            });
        });
}